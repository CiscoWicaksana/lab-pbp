1. Apakah perbedaan antara JSON dan XML?

Yang pertama adalah
JSON (JavaScript Object Notation) itu berdasar pada JavaScript
sedangkan XML (Extensible Markup Language) itu berdasar dari SGML.
JSON digunakan untuk merepresentasikan object, dan tidak bisa menggunakan
namespace & comment, tapi bisa menggunakan array. Sedangkan XML adalah 
markup language dengan struktur tag untuk representasi data, bisa menggunakan
namespace, serta comment, tapi tidak bisa menggunakan array. Selain itu, XML
perlu menggunakan End tag, sedangkan JSON tidak. XML juga lebih tua (dikembangkan lebih awal)
daripada JSON, makanya JSON biasanya lebih singkat.

2. Apakah perbedaan antara HTML dan XML?

HTML = Hypertext Markup Language
XML = Extensible Markup Language

HTML lebih fokus untuk menentukan desain dan struktur informasi yang 
ditampilkan. 
Sedangkan XML lebih foksu untuk menyimpan, mentransfer, menstruktur data
dan menjelaskan kegunaan data.
