from django.urls import path

from lab_2 import views
from .views import index

urlpatterns = [
    path('', index, name='index'),
    path('notes', views.index),
    path('xml', views.xml),
    path('json', views.json)
]
