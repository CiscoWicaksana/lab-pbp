from django.contrib import admin


# Register your models here.
from lab_2.models import Note


class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Note, AuthorAdmin)
