from django.db import models


# Create your models here.
class Note(models.Model):
    to = models.CharField(max_length=30)
    sender = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.CharField(max_length=300)

    def __str__(self):
        return f"{self.to}  {self.sender}  {self.title} {self.message}"
