from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponse
from django.core import serializers

to = 'Cisco Salya Wicaksana'
sender = 'Cisco Salya Wicaksana'
title = 'Test Note'
message = 'Test Message'


# Create your views here.
def index(request):
    notes = Note.objects.all()
    response = {'to': to,
                'from': sender,
                'title': title,
                'message': message,
                'notes': notes}
    print(notes)
    return render(request, 'index_lab2.html', response)


def xml(request):
    data = serializers.serialize('xml', Note.objects.all())

    return HttpResponse(data, content_type="application/xml")


def json(request):
    notes = Note.objects.all()
    data = serializers.serialize('json', notes)

    return HttpResponse(data, content_type="application/json")