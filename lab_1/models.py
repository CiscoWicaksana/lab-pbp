from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    dob = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.name}  {self.npm}  {self.dob}"
