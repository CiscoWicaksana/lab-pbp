from django.contrib import admin

from lab_1.models import Friend


class AuthorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Friend, AuthorAdmin)
