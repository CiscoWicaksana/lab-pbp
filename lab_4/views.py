from django.shortcuts import render, redirect
from django.http.response import HttpResponse
from .forms import ContactForm, SnippetForm
# from lab_2.models import Note
# from django.core import serializers
#
# from lab_4.forms import NoteForm
#
#
# # Create your views here.
#


def contact(request):

    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():

            to = form.cleaned_data['to']
            print(to)

    form = ContactForm()
    return render(request, "lab4_form.html", {'form': form})


def snippet_detail(request):

    if request.method == 'POST':
        form = SnippetForm(request.POST)
        if form.is_valid():
            print("VALID")
            form.save()

    form = SnippetForm()
    return render(request, "lab4_form.html", {'form': form})

# def index(request):
#     notes = Note.objects.all()
#     data = {"daftar_note": notes}
#     return render(request, 'lab4_index.html', context=data)
#
#
# def form(request):
#     forms = NoteForm()
#     return HttpResponse(request, "lab4_form.html", {"form": forms})
#
#
# def add_note(request):
#     if request.method == "POST":
#         form = NoteForm(request.POST)
#         if form.is_valid():
#             form.save()
#     return redirect("/lab-4")
#
