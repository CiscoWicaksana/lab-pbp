from django.urls import path
from . import views
# from .views import index, form

urlpatterns = [
    # path('', form, name="index_lab4"),
    # path('add_note/', add_note, name="add_note")
    path('', views.contact),
    path('snippet', views.snippet_detail)
]
