from django.forms import ModelForm
from django import forms
# from crispy_forms.helper import FormHelper
# from crispy_forms.layout import Layout, Submit

# from lab_2.models import Note
#
#
# class NoteForm(ModelForm):
#     class Meta:
#         model = Note
#         fields = ['to', 'sender', 'title', 'message']
from lab_4.models import Snippet


class ContactForm(forms.Form):
    to = forms.CharField()
    sender = forms.CharField()
    title = forms.CharField()
    message = forms.CharField(widget=forms.Textarea)
    #
    # def __init__(self, *args, **kwargs):
    #     super().__init__(self, *args, **kwargs)
    #
    #     self.helper = FormHelper
    #     self.helper.form_method = 'post'


class SnippetForm(ModelForm):
    class Meta:
        model = Snippet
        fields = ('name', 'body')
