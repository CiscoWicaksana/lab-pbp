from django.urls import path

from . import views
from .views import index, add_friend, form

urlpatterns = [
    path('', index, name='index'),
    path('add/', form, name="add_friend_form"),
    path('add/submit', add_friend, name="add_friend")
]
