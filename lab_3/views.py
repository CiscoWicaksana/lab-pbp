from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from lab_3.models import Friend
from lab_3.forms import FriendForm


def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    print(friends)
    return render(request, 'index_lab3.html', response)


def form(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    print(friends)
    return render(request, 'lab3_form.html', response)


# @login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
    return redirect(index)
